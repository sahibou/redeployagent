package com.tet.test.agentdir;

import java.awt.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.commons.io.IOUtils;

/**
 * Hello world!
 *
 */
public class AgentDir {
	public static void premain(String args, Instrumentation instrumentation)
			throws ClassNotFoundException, IOException, UnmodifiableClassException, InterruptedException {
		System.out.println("MECO !!" + instrumentation);

		Thread t = new Thread(new AgentThread().init(instrumentation));
		t.start();
	}
	
//	public static void agentmain(String args, Instrumentation instrumentation)
//			throws ClassNotFoundException, IOException, UnmodifiableClassException, InterruptedException {
//		System.out.println("SECO !!" + instrumentation);
//
//		Thread t = new Thread(new AgentThread().init(instrumentation));
//		t.start();
//	}
}
