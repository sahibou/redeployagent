package com.tet.test.agentdir;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;

import static java.nio.file.StandardWatchEventKinds.*;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

public class AgentThread implements Runnable {

	private Instrumentation inst = null;
	private URLClassLoader classloader = null;

	WatchService watchService;

	public AgentThread init(Instrumentation inst) {
		// Instrumentation
		this.inst = inst;
		// ClassLoader
		try {
			URL[] urls = { new URL("file:///c:/dev/workspaces/w_agent_dir/Target/target/classes/") };
			classloader = new URLClassLoader(urls);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}

		return this;
	}

	@Override
	public void run() {

		while (!Thread.currentThread().isInterrupted()) {
			try {

				FileAlterationObserver observer = new FileAlterationObserver("c:/dev/workspaces/w_agent_dir/Target/target/classes/");
				FileAlterationMonitor monitor = new FileAlterationMonitor(5000);
				FileAlterationListener listener = new FileAlterationListenerAdaptor() {
					@Override
					public void onFileCreate(File file) {
						System.out.println("File: " + file.getName() + " created");
					}

					@Override
					public void onFileDelete(File file) {
						System.out.println("File: " + file.getName() + " deleted");
					}

					@Override
					public void onFileChange(File file) {
						System.out.println("File: " + file.getPath() + " changed");
try {
	Thread.sleep(5000);
} catch (InterruptedException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}						
//						Class<?> Babar = classloader.loadClass(dirname);
//
//						FileInputStream fio = new FileInputStream(eventPathItem.toFile());
//						byte[] classbytes = IOUtils.toByteArray(fio);
//						fio.close();
//
//						ClassDefinition cd = new ClassDefinition(Babar, classbytes);
//						inst.redefineClasses(cd);
					}
				};
				observer.addListener(listener);
				monitor.addObserver(observer);
				monitor.start();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
