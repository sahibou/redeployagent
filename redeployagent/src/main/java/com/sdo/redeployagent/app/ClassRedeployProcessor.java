package com.sdo.redeployagent.app;

import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

import org.apache.log4j.Logger;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class ClassRedeployProcessor implements Runnable{

	private static final Logger log = Logger.getLogger(ClassRedeployAgentBootstrap.class);


	private Instrumentation inst=null;
	/**
	 * Sets the Instrumentation for later use.
	 * @param inst
	 * @return
	 */
	public ClassRedeployProcessor init(Instrumentation inst) {
		this.inst=inst;
		return this;
	}
	
	/**
	 * Update class definition when a ClassDefinition is received
	 * @param cdef
	 */
	public void redefineVirtualMachineClass(ClassDefinition cdef) {
		try {
			inst.redefineClasses(new ClassDefinition[] {cdef});
		} catch (ClassNotFoundException e) {
			log.error("Redefine couldnt find class "+cdef);
		} catch (UnmodifiableClassException e) {
			log.error("Redefine couldnt access class "+cdef);
		}		
	}

	@Override
	public void run() {
		//WAIT 0MQ
		//GET CD
		//UDPDATE CD
		//RESTART
		long start = System.currentTimeMillis();
		ZContext context = new ZContext();
		while(true) {
            // Socket to talk to clients
            ZMQ.Socket socket = context.createSocket(ZMQ.REP);
            socket.bind("tcp://*:6006");

            while (!Thread.currentThread().isInterrupted()) {
                byte[] reply = socket.recv(0);
                
                String response = "AGENT Redeployed class in "+(System.currentTimeMillis()-start+"ms");
                socket.send(response.getBytes(ZMQ.CHARSET), 0);

                //Thread.sleep(100); //  Do some 'work'
            }
        }
	}
	
	
}
