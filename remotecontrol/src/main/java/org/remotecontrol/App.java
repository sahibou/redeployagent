package org.remotecontrol;

import org.remotecontrol.fileeventprocessor.FileEventProcessor;
import org.remotecontrol.filemonitor.FileUpdateMonitor;
import org.remotecontrol.utils.ClassUtils;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		ClassUtils cu = new ClassUtils(args);
		
		FileUpdateMonitor mon = new FileUpdateMonitor();

		mon.addObserver(new FileEventProcessor(cu));
		mon.run(args);
	}
}