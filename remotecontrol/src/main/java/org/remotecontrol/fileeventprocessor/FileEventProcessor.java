package org.remotecontrol.fileeventprocessor;

import java.io.File;
import java.lang.instrument.ClassDefinition;
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;
import org.remotecontrol.utils.ClassUtils;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class FileEventProcessor implements Observer {

	private static final Logger log = Logger.getLogger(FileEventProcessor.class);
	private static final ZContext context = new ZContext();
	
	private ClassUtils cu = null;
	public FileEventProcessor(ClassUtils cu){
		this.cu=cu;
	}
	
	public void update(Observable o, Object arg) {
		File f = (File)arg;
		//CreateClassDefinition		
		log.info(cu);
		Class<?> theClass = cu.getClassFromFile(f.toString());
		byte[] theClassFile = cu.getClassBytes(f);
		
		ClassDefinition cd = new ClassDefinition(theClass, theClassFile);
		pushClassDefinition(cd);
	}

	private void pushClassDefinition(ClassDefinition cd) {
		log.info("@@@@@@@ @@@ SENDING CDEF "+cd);
        //  Socket to talk to server
        System.out.println("Connecting to hello world server");

        ZMQ.Socket socket = context.createSocket(ZMQ.REQ);
        socket.connect("tcp://localhost:6006");

        socket.send(("CDEF"+cd.hashCode()).getBytes(ZMQ.CHARSET), 0);

        byte[] reply = socket.recv(0);
        System.out.println(
            "Received " + new String(reply, ZMQ.CHARSET)
        );
        
        
	}
	
}
